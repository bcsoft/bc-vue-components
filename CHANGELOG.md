# BC 平台的 vue 组件发布历史

## 0.1.0 - 2016-07-08 
- demo at [http://127.0.0.1:3000/examples/index.html]()
- add bc-theme component
- add bc-button component
- add bc-button-set component
- add bc-search component
- add bc-toolbar component
- add bc-loading component
- add bc-page-bar component
- add bc-table-col component
- add bc-grid component