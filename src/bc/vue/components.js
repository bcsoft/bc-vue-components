/*! BC 平台的 vue 组件
 * @author dragon <rongjihuang@gmail.com>
 * @version 0.1.0 2016-07-08
 * @license Apache License 2.0
 * @components bc-theme
 *             bc-button
 *             bc-button-set
 *             bc-search
 *             bc-toolbar
 *             bc-table-col
 *             bc-page-bar
 *             bc-loading
 *             bc-grid
 */
define(["bc/vue/theme", "bc/vue/button", "bc/vue/button-set", "bc/vue/search"
	, "bc/vue/toolbar"
	, "bc/vue/table-col", "bc/vue/page-bar", "bc/vue/loading"
	, "bc/vue/grid"
], function () {
	return 0;
});